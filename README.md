# 2022年度・千葉大学・認知情報科学基礎実習・恒常性に関する実験レポート

スクリプト実行するには、
```bash
Rscript sizeConst.R
```

T検定の結果可視化用に、gginferenceを使用しているため、
インストール済みでない場合、以下の実行が必要である。

```R
install.packages("gginference")
```



